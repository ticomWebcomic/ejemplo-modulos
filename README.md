# Ejemplo de módulos en ES6

Una demostración de como crear y usar módulos ES6, estos estan en la carpeta de modulos y son llamados e inicializados por index.js

## Referencias

[A Practical guide to ES6 modules](https://www.freecodecamp.org/news/how-to-use-es6-modules-and-why-theyre-important-a9b20b480773/)

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
