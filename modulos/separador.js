class Separador {
  constructor(tArea) {
    this.areaTexto = tArea;
  }

  separadorMas() {
    var texto = this.areaTexto.value;
    texto = texto + "++++++++++" + "\n";

    this.areaTexto.value = texto;
  }

  separadorMenos() {
    var texto = this.areaTexto.value;
    texto = texto + "----------" + "\n";

    this.areaTexto.value = texto;
  }
}

export { Separador };
