class Contador {
  constructor(tArea) {
    this.conteo = 0;
    this.texto = tArea;
  }

  incrementar() {
    console.log("Incrementar");
    this.conteo++;

    var texto = this.texto.value;

    texto = texto + this.conteo + "\n";

    this.texto.value = texto;
  }

  decrementar() {
    this.conteo--;
    var texto = this.texto.value;
    texto = texto + this.conteo + "\n";

    this.texto.value = texto;
  }
}

export { Contador };
