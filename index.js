import { Contador } from "./modulos/contador.js";
import { Separador } from "./modulos/separador.js";

var areaTexto = document.getElementById("salida");

var accionContador = new Contador(areaTexto);
var accionSeparador = new Separador(areaTexto);

var botonIncrementar = document.getElementById("botonContador");
var botonDecrementar = document.getElementById("botonRestador");

botonIncrementar.addEventListener("click", function() {
  accionContador.incrementar();
  accionSeparador.separadorMas();
});

botonDecrementar.addEventListener("click", function() {
  accionContador.decrementar();
  accionSeparador.separadorMenos();
});
